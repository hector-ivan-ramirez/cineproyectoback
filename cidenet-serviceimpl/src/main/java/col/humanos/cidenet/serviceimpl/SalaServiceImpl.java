package col.humanos.cidenet.serviceimpl;

import col.humanos.cidenet.SalaPuestoService;
import col.humanos.cidenet.SalaRepository;
import col.humanos.cidenet.SalaService;
import col.humanos.cidenet.dto.SalaDTO;
import col.humanos.cidenet.enums.SucursalCiudadEnum;
import col.humanos.cidenet.enums.TipoSalaEnum;
import col.humanos.cidenet.model.Sala;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class SalaServiceImpl implements SalaService {

  /** The Constant LOGGER. */
  private static final Logger LOGGER = LoggerFactory.getLogger(SalaServiceImpl.class);

  private SalaRepository salaRepository;

  private SalaPuestoService salaPuestoService;

  @Autowired
  public SalaServiceImpl(SalaRepository salaRepository,SalaPuestoService salaPuestoService) {
    this.salaRepository=salaRepository;
    this.salaPuestoService=salaPuestoService;
  }

  @Override
  public List<SalaDTO> busquedaAvanzada() {

    return  converSalaASalaDto(salaRepository.findAll());
  }

  @Override
  public void crearSala(SalaDTO salaDto){
    Sala sala=converSalaDtoASala(salaDto);
    salaRepository.save(sala);
    salaPuestoService.crearSalaPuestos(salaDto.getSalaSilla(),sala.getId());
  }

  private List<SalaDTO> converSalaASalaDto(List<Sala> listSala){
    List<SalaDTO> listSalaDto= new ArrayList<>();
    if(null != listSala && !CollectionUtils.isEmpty(listSala)) {
      for (Sala sala: listSala) {
        SalaDTO salaDTO=new SalaDTO();
        salaDTO.setCodSucursalCiudad(sala.getCodSucursalCiudad());
        salaDTO.setId(sala.getId());
        salaDTO.setNroMaximoSillaFila(sala.getNroMaximoSillaFila());
        salaDTO.setNumeroFila(sala.getNumeroFila());
        salaDTO.setNomSucursal(SucursalCiudadEnum.getById(sala.getCodSucursalCiudad().intValue()).getNombre());
        salaDTO.setNomTipoSala(TipoSalaEnum.getById(sala.getCodTipoSala().intValue()).getNombre());
        listSalaDto.add(salaDTO);
      }
    }

    return listSalaDto;
  }

  private Sala converSalaDtoASala(SalaDTO salaDto){
    Sala sala= new Sala();
    sala.setCodSucursalCiudad(salaDto.getCodSucursalCiudad());
    sala.setCodTipoSala(salaDto.getCodTipoSala());
    sala.setNroMaximoSillaFila(salaDto.getNroMaximoSillaFila());
    sala.setNumeroFila(salaDto.getNumeroFila());

    return sala;
  }
}
