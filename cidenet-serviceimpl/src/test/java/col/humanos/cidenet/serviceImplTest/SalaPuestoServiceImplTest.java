package col.humanos.cidenet.serviceImplTest;

import col.humanos.cidenet.SalaPuestoRepository;
import col.humanos.cidenet.SalaPuestoService;
import col.humanos.cidenet.serviceimpl.SalaPuestoServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;

@RunWith(MockitoJUnitRunner.class)
public class SalaPuestoServiceImplTest {

  @Autowired
  private SalaPuestoService salaPuestoService;

  @Mock
  private SalaPuestoRepository salaPuestoRepository;

  @Before
  public void setup(){
    salaPuestoService=new SalaPuestoServiceImpl(salaPuestoRepository);
  }


  @Test
  public void crearSalaPuestos(){

    String salaPuesto="A1|A2";
    salaPuestoService.crearSalaPuestos(salaPuesto, BigInteger.valueOf(1));
  }

  @Test
  public void crearSalaPuestos1(){

    String salaPuesto="A1|A2\r\nB1|B2";
    salaPuestoService.crearSalaPuestos(salaPuesto, BigInteger.valueOf(1));
  }
}

