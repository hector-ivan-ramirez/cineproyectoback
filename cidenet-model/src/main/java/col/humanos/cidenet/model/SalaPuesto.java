package col.humanos.cidenet.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_sala_puesto")
public class SalaPuesto implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALA_PUE_SEQ")
  @SequenceGenerator(name="SALA_PUE_SEQ",sequenceName = "tb_sala_puesto_cod_sala_puesto_seq",allocationSize=1)
  @Column(name = "cod_sala_puesto", nullable = false)
  private BigInteger id;

  @Column(name = "cod_sala")
  private BigInteger codSala;

  @Column(name = "letra_fila_consecutivo")
  private String numeroFilaConsecutivo;

  @Column(name = "estado")
  private String estado;

}
