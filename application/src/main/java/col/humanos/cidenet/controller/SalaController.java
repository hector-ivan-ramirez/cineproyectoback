package col.humanos.cidenet.controller;

import col.humanos.cidenet.SalaService;
import col.humanos.cidenet.dto.SalaDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;


@RestController
@CrossOrigin(origins="http://localhost:4200")
@RequestMapping(value="/api")
public class SalaController {

  private SalaService salaService;

  @Autowired
  public SalaController(SalaService salaService) {
    this.salaService=salaService;
  }

  @GetMapping("room")
  public List<SalaDTO> detalleSalas() {
    return salaService.busquedaAvanzada();
  }


  @PostMapping("saveroom")
  public void saveStudent(@RequestBody SalaDTO salaDto) {
    salaService.crearSala(salaDto);
  }




}
