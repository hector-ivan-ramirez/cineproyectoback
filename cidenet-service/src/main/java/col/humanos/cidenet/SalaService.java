package col.humanos.cidenet;

import col.humanos.cidenet.dto.SalaDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface SalaService {


  List<SalaDTO> busquedaAvanzada();

  @Transactional
  void crearSala(SalaDTO salaDto);
}
