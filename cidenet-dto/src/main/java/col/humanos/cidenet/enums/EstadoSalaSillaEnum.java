package col.humanos.cidenet.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public enum EstadoSalaSillaEnum {

  LIBRE("L","Libre"),OCUPADO("O","Ocupado"),RESERVADO("R","Reservado"),DESCONOCIDO("D","Desconocido");

  private final String id;
  private final String nombre;


  private EstadoSalaSillaEnum(String id, String nombre) {
    this.id = id;
    this.nombre = nombre;
  }

  public static Stream<EstadoSalaSillaEnum> stream() {
    return Stream.of(EstadoSalaSillaEnum.values());
  }

  public static EstadoSalaSillaEnum getById(String id) {
    for (EstadoSalaSillaEnum e : values()) {
      if (StringUtils.equals(e.getId(),id) ) {
        return e;
      }
    }
    return DESCONOCIDO;
  }

  public String getId() {
    return id;
  }

  public String getNombre() {
    return nombre;
  }
}
