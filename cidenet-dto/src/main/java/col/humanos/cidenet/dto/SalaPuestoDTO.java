package col.humanos.cidenet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SalaPuestoDTO implements Serializable {

  private BigInteger id;

  private BigInteger codSala;

  private String numeroFilaConsecutivo;

  private String estado;
}
