package col.humanos.cidenet;

import col.humanos.cidenet.model.SalaPuesto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface SalaPuestoRepository extends JpaRepository<SalaPuesto, BigInteger> {

}
